package com.map.kasim.map;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Date;

public class MapsActivity extends FragmentActivity {
    // This piece of code shows that it has the maps activity that extends the fragment activity which implements on map ready call back.

    private GoogleMap mMap;

    private SensorManager sensorManager;
    private Sensor accelerometer;
    // this feature is used to use the accelerometer in the phone to determine if anyone has moved the device.
    private float last_x, last_y, last_z;
    long lastUpdate = 0;

    private GoogleApiClient client;

    public MapsActivity(GoogleMap mMap) {
        this.mMap = mMap;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // The on create method is used essentially to tell the phone what to do when the app has loaded up
        super.onCreate(savedInstanceState);
        //What this does is create a saved instance state which sets the content view to activity maps.
        setContentView(R.layout.activity_maps);
        setUpMapIfNeeded();
        // In other words this means is that when the phone loads up it shows the activity maps layout, and then set up the map if needed.

        // This is were I would like to set up the accelerometer
        sensorManager = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener((SensorEventListener) this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        // This piece of code basically sets up the sensor manager that we have declared above its from gets system service, sensor service.
        // The accelerometer is then getting the sensor manager and getting the sensor and I want it type accelerometer.
        // Then to have something to listen I have to register a listener, so i told the app that it needs to register this listener so we know it should be listening for the accelerometer.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void onSensorChanged(SensorEvent event) { // this will be my listener it will say when the sensor has changed.
        Sensor mySensor = event.sensor;

        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = event.values[0]; //This is the x value
            float y = event.values[1]; //This is the y Value
            float z = event.values[2]; //This is the z Value
            // this will make sure the sensor tht we are getting from the app is the accelerometer.
            // if it is i get the x y and z values.

            long curTime = System.currentTimeMillis(); // this will set a time to make sure it update within a accurate timing.

            if (Math.abs(curTime - lastUpdate) > 2000) {
                SimpleDateFormat date = new SimpleDateFormat("dd:mm:yyyy");
                String currentDateTime = date.format(new Date());
                // this code shows that when the phone is moving if it is in the last 2 second i would like to find the date on the phone

                lastUpdate = curTime;

                if (Math.abs(last_x - x) > 10) // this calculates if the x coordinate is greater than 10
                {
                    mMap.addMarker(new MarkerOptions() // This code is to create a marker
                            .position(new LatLng(52.567293, -1.499675)) //using this latitude and longitude
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)) // This is for the colour being Red
                            .title("Faculty of Engineering, Environment and Computing" +
                                    "Our innovative activity-led learning approach is internationally recognised and has shaped the unique culture of our forward-facing Faculty in its £55m state-of-the art building. We’re inspiring people to study in a broad range of disciplines, in order to widen perception about how engineering and computing can impact on worldwide cultural development. Through original approaches from world-leading experts, we aim for our research to make a tangible difference to the way we live. The Faculty encourages PhD studentships and internships to run across all areas of study - we are proud to reinvest in our students and graduates."));

                }

                if (Math.abs(last_y - y) > 10) // this calculates if the x coordinate is greater than 10
                {
                    mMap.addMarker(new MarkerOptions() // This code is to create a marker
                            .position(new LatLng(52.454693, -1.494575)) //using this latitude and longitude
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)) // This is for the colour being Blue
                            .title("Faculty of Engineering, Environment and Computing" +
                                    "Our innovative activity-led learning approach is internationally recognised and has shaped the unique culture of our forward-facing Faculty in its £55m state-of-the art building. We’re inspiring people to study in a broad range of disciplines, in order to widen perception about how engineering and computing can impact on worldwide cultural development. Through original approaches from world-leading experts, we aim for our research to make a tangible difference to the way we live. The Faculty encourages PhD studentships and internships to run across all areas of study - we are proud to reinvest in our students and graduates."));

                }

                if (Math.abs(last_z - z) > 10) // this calculates if the x coordinate is greater than 10
                {
                    mMap.addMarker(new MarkerOptions() // This code is to create a marker
                            .position(new LatLng(52.769293, -1.456675)) //using this latitude and longitude
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)) // This is for the colour being Orange
                            .title("Faculty of Engineering, Environment and Computing" +
                                    "Our innovative activity-led learning approach is internationally recognised and has shaped the unique culture of our forward-facing Faculty in its £55m state-of-the art building. We’re inspiring people to study in a broad range of disciplines, in order to widen perception about how engineering and computing can impact on worldwide cultural development. Through original approaches from world-leading experts, we aim for our research to make a tangible difference to the way we live. The Faculty encourages PhD studentships and internships to run across all areas of study - we are proud to reinvest in our students and graduates."));

                }

                last_x = x;
                last_y = y;
                last_z = z;

            }
        }
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) { // this is needed for the sensor accuracy

    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {

            MapFragment mapFragment = (MapFragment) getFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync((OnMapReadyCallback) this);
            if (mMap != null) {
                setUpMap();

            }

        }
    }

    private void setUpMap() {
        Marker coventry = mMap.addMarker(new MarkerOptions().position(new LatLng(52.769293, -1.456675)).title("Coventry"));
    }
}